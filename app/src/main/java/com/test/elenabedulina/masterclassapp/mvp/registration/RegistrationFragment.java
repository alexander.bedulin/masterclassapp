package com.test.elenabedulina.masterclassapp.mvp.registration;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.test.elenabedulina.masterclassapp.R;
import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragment;
import com.test.elenabedulina.masterclassapp.mvp.login.LoginFragment;
import com.test.elenabedulina.masterclassapp.mvp.main.MainActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class RegistrationFragment extends BaseFragment implements RegistrationView {
    private FragmentTransaction fTrans;
    @InjectPresenter
    RegistrationPresenter mvpPresenter;
    @BindView(R.id.ed_email_registration)
    EditText edEmail;
    @BindView(R.id.ed_password_registration)
    EditText edPassword;

    @Override
    public int getResourceId() {
        return R.layout.activity_registration;
    }

    @Override
    public void openLoginScreen() {
        fTrans = getActivity().getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frame_layout, new LoginFragment())
                .commit();
    }

    @Override
    public void openMainScreen() {
        getActivity().finish();
        startActivity(new Intent(getContext(), MainActivity.class));
    }

    @Override
    public void showMessageEmailIsRegistered() {
        Toast.makeText(getContext(), R.string.email_is_registered, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyEmailMessage() {
        edEmail.setError(getString(R.string.no_email));
    }

    @Override
    public void showEmptyPasswordMessage() {
        edPassword.setError(getString(R.string.no_password));
    }

    @Override
    public void showWrongPasswordMessage() {
        edPassword.setError(getString(R.string.password_less_then_four));
    }

    @OnClick(R.id.btn_registration)
    void onRegistrationClick() {
        mvpPresenter.onRegistrationClick(edEmail.getText().toString(), edPassword.getText().toString());
    }

    @OnClick(R.id.tv_already_registered)
    void onAlreadyRegisteredClick() {
        mvpPresenter.onAlreadyRegisteredClick();
    }
}
