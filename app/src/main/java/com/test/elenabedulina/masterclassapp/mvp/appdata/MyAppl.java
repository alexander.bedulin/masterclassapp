package com.test.elenabedulina.masterclassapp.mvp.appdata;

import android.app.Application;
import android.content.Context;

import javax.inject.Inject;

public class MyAppl extends Application {

    @Inject
    DataManager dataManager;
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public static MyAppl get(Context context) {
        return (MyAppl) context.getApplicationContext();
    }

}