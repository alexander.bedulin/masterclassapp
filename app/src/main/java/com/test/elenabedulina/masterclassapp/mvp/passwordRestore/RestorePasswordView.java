package com.test.elenabedulina.masterclassapp.mvp.passwordRestore;

import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragmentView;

public interface RestorePasswordView extends BaseFragmentView {

    void showEmailIsWrongMessage();

    void showCheckEmailMessage();

    void showEmptyEmailMessage();
}
