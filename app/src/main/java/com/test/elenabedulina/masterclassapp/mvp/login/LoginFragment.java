package com.test.elenabedulina.masterclassapp.mvp.login;

import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.test.elenabedulina.masterclassapp.R;
import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragment;
import com.test.elenabedulina.masterclassapp.mvp.main.MainActivity;
import com.test.elenabedulina.masterclassapp.mvp.passwordRestore.RestorePasswordFragment;
import com.test.elenabedulina.masterclassapp.mvp.registration.RegistrationFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment implements LoginView {

    @InjectPresenter
    LoginPresenter mvpPresenter;
    @BindView(R.id.ed_email_login)
    EditText edEmail;
    @BindView(R.id.ed_password_login)
    EditText edPassword;

    @OnClick(R.id.forgot_password)
    void onForgotPasswordClick() {
        mvpPresenter.onForgotPasswordClick();
    }

    @OnClick(R.id.btn_sign_up)
    void onSignUpClick() {
        mvpPresenter.onSignUpClick();
    }

    @Override
    public int getResourceId() {
        return R.layout.activity_login;
    }

    @Override
    public void openForgotPasswordScreen() {
        getFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, new RestorePasswordFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void openMainScreen() {
        getActivity().finish();
        startActivity(new Intent(getContext(), MainActivity.class));
    }

    @Override
    public void openRegistrationScreen() {
        getFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, new RegistrationFragment())
                .remove(this)
                .commit();
    }

    @Override
    public void showWrongPasswordMessage() {
        Toast.makeText(getContext(), R.string.wrong_email_password, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyEmailMessage() {
        edEmail.setError(getString(R.string.no_email));
    }

    @Override
    public void showEmptyPasswordMessage() {
        edPassword.setError(getString(R.string.no_password));
    }

    @OnClick(R.id.btn_login)
    void onLoginBtnClick() {
        mvpPresenter.onLoginClick(edEmail.getText().toString(), edPassword.getText().toString());
    }

}
