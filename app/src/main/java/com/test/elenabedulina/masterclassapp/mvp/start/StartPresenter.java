package com.test.elenabedulina.masterclassapp.mvp.start;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.auth.FirebaseUser;

@InjectViewState
public class StartPresenter extends MvpPresenter<StartView> {

    public void openNewScreen(FirebaseUser firebaseUser) {
        if (firebaseUser != null) {
            getViewState().openMainScreen();
        } else {
            getViewState().openLoginScreen();
        }
    }
}
