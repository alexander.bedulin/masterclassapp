package com.test.elenabedulina.masterclassapp.mvp.baseMVP;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.auth.FirebaseAuth;

@InjectViewState
public class BaseActivityPresenter extends MvpPresenter<BaseActivityView> {
    private final int SLEEPING_TIME = 5;

    public void onActivityCreate(long startSleepingTime, FirebaseAuth firebaseAuth) {
        if (calculateSleepingTime(System.currentTimeMillis(), startSleepingTime) >= SLEEPING_TIME) {
            firebaseAuth.signOut();
            getViewState().openStartScreen();
        }
    }

    private long calculateSleepingTime(long currentTime, long startSleepingTime) {
        if (startSleepingTime == 0) {
            return 0;
        } else {
            return ((currentTime - startSleepingTime) / (1000 * 60)) % 60;
        }
    }
}