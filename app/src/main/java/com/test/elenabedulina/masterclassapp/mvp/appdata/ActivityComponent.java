package com.test.elenabedulina.masterclassapp.mvp.appdata;

import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);
}