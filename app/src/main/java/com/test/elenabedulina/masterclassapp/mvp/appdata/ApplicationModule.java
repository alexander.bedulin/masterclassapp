package com.test.elenabedulina.masterclassapp.mvp.appdata;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final Application moduleApplication;

    public ApplicationModule(Application app) {
        moduleApplication = app;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return moduleApplication;
    }

    @Provides
    Application provideApplication() {
        return moduleApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return "demo-dagger.db";
    }

    @Provides
    @DatabaseInfo
    Integer provideDatabaseVersion() {
        return 2;
    }

    @Provides
    SharedPreferences sharedPreferences() {
        return moduleApplication.getSharedPreferences("sharedPreferencesProvider", Context.MODE_PRIVATE);
    }
}