package com.test.elenabedulina.masterclassapp.mvp.baseMVP;

import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.auth.FirebaseAuth;

public class BaseFragmentPresenter<T extends BaseFragmentView> extends MvpPresenter<T> {

    public FirebaseAuth initFarebase(){
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        return firebaseAuth;
    }

}
