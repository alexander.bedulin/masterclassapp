package com.test.elenabedulina.masterclassapp.mvp.baseMVP;

import com.arellomobile.mvp.MvpView;

public interface BaseActivityView extends MvpView {

    void  openStartScreen();
}