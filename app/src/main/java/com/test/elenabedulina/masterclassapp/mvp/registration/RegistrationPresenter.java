package com.test.elenabedulina.masterclassapp.mvp.registration;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragmentPresenter;

@InjectViewState
public class RegistrationPresenter extends BaseFragmentPresenter<RegistrationView> {
    private static final int PASSWORD_MIN_LENGTH = 4;

    void onAlreadyRegisteredClick() {
        getViewState().openLoginScreen();
    }

    void onRegistrationClick(@NonNull String email, @NonNull String password) {
       if(checkInput(email,password )){
           signUpNewUser(email,password );
       }
    }

    private boolean checkInput(@NonNull String email, @NonNull String password){
        boolean checkEditTexts = true;
        if (email.isEmpty()) {
            getViewState().showEmptyEmailMessage();
            checkEditTexts = false;
        }
        if (password.isEmpty()) {
            getViewState().showEmptyPasswordMessage();
            checkEditTexts = false;
        } else if (password.length() <= PASSWORD_MIN_LENGTH) {
            getViewState().showWrongPasswordMessage();
            checkEditTexts = false;
        }
        return checkEditTexts;
    }

    private void signUpNewUser(@NonNull String email, @NonNull String password){
        getViewState().showProgressBar();
        initFarebase().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    getViewState().hideProgressBar();
                    if (task.isSuccessful()) {
                        getViewState().openMainScreen();
                    } else if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        getViewState().showMessageEmailIsRegistered();
                    } else {
                        getViewState().showErrorMessage();
                    }
                });
    }
}
