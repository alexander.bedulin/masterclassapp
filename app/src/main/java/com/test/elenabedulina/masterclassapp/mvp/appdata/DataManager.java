package com.test.elenabedulina.masterclassapp.mvp.appdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.test.elenabedulina.masterclassapp.mvp.sharedpreferences.Preferences;
import com.test.elenabedulina.masterclassapp.mvp.sharedpreferences.SharedPreferencesProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataManager {
    private SharedPreferencesProvider sharedPreferencesProvider;
    private Context context;

    @Inject
    public DataManager(@ApplicationContext Context context, SharedPreferencesProvider sharedPreferencesProvider) {
        this.context = context;
        this.sharedPreferencesProvider = sharedPreferencesProvider;
    }

    public Context getContext() {
        return context;
    }

    public Preferences createPreferences(@NonNull SharedPreferences sharedPreferences) {
        return new SharedPreferencesProvider(sharedPreferences);
    }

    public void setStartSleepingTime(long startSleepingTime) {
        this.sharedPreferencesProvider.setStartSleepingTime(startSleepingTime);
    }

    public long getStartSleepingTime() {
        return this.sharedPreferencesProvider.getStartSleepingTime();
    }

}