package com.test.elenabedulina.masterclassapp.mvp.baseMVP;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.test.elenabedulina.masterclassapp.R;
import com.test.elenabedulina.masterclassapp.mvp.appdata.ActivityComponent;
import com.test.elenabedulina.masterclassapp.mvp.appdata.ActivityModule;
import com.test.elenabedulina.masterclassapp.mvp.appdata.DaggerActivityComponent;
import com.test.elenabedulina.masterclassapp.mvp.appdata.MyAppl;
import com.test.elenabedulina.masterclassapp.mvp.appdata.Presenter;
import com.test.elenabedulina.masterclassapp.mvp.start.StartActivity;

import javax.inject.Inject;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseActivityView {
    @InjectPresenter
    BaseActivityPresenter mvpPresenter;
    @Inject
    Presenter presenter;

    private ActivityComponent activityComponent;

    public ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(MyAppl.get(this).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_layout);
        getActivityComponent().inject(this);
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        mvpPresenter.onActivityCreate(presenter.getStartSleepingTime(), firebaseAuth);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.setStartSleepingTime(System.currentTimeMillis());
    }

    @Override
    public void openStartScreen() {
        startActivity(new Intent(this, StartActivity.class));
    }
}
