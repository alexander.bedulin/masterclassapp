package com.test.elenabedulina.masterclassapp.mvp.baseMVP;

import com.arellomobile.mvp.MvpView;

public interface BaseFragmentView extends MvpView {

    void showProgressBar();

    void hideProgressBar();

    void showErrorMessage();
}
