package com.test.elenabedulina.masterclassapp.mvp.start;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.test.elenabedulina.masterclassapp.R;
import com.test.elenabedulina.masterclassapp.mvp.login.LoginFragment;
import com.test.elenabedulina.masterclassapp.mvp.main.MainActivity;

public class StartActivity extends MvpAppCompatActivity implements StartView {

    @InjectPresenter
    StartPresenter mvpPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_layout);
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        mvpPresenter.openNewScreen(firebaseUser);
    }

    @Override
    public void openLoginScreen() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_layout, new LoginFragment())
                .commit();
    }

    @Override
    public void openMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
    }
}
