package com.test.elenabedulina.masterclassapp.mvp.sharedpreferences;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class SharedPreferencesProvider implements Preferences {
    public static String START_SLEEPING_TIME = "startSleepingTime";
    private final SharedPreferences msharedPreferences;

    @Inject
    public SharedPreferencesProvider(SharedPreferences sharedPreferences) {
        msharedPreferences = sharedPreferences;
    }

    @Override
    public void setStartSleepingTime(long startSleepingTime) {
        msharedPreferences.edit().putLong(START_SLEEPING_TIME , startSleepingTime).apply();
    }

    @Override
    public long getStartSleepingTime() {
        return msharedPreferences.getLong( START_SLEEPING_TIME, 0);
    }
}
