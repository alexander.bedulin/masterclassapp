package com.test.elenabedulina.masterclassapp.mvp.registration;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragmentView;

public interface RegistrationView extends BaseFragmentView {
    @StateStrategyType(SkipStrategy.class)
    void openLoginScreen();

    @StateStrategyType(SkipStrategy.class)
    void openMainScreen();

    void showMessageEmailIsRegistered();

    void showEmptyEmailMessage();

    void showEmptyPasswordMessage();

    void showWrongPasswordMessage();

}
