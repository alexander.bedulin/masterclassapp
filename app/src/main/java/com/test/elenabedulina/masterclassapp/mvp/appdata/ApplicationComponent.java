package com.test.elenabedulina.masterclassapp.mvp.appdata;

import android.app.Application;
import android.content.Context;

import com.test.elenabedulina.masterclassapp.mvp.sharedpreferences.SharedPreferencesProvider;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MyAppl myAppl);

    @ApplicationContext
    Context getContext();

    Application getApplication();

    DataManager getDataManager();

    SharedPreferencesProvider getSharedPreferencesProvider();

}