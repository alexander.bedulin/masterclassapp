package com.test.elenabedulina.masterclassapp.mvp.appdata;

import com.test.elenabedulina.masterclassapp.mvp.sharedpreferences.SharedPreferencesProvider;

import javax.inject.Inject;

public class Presenter {

    @Inject
    SharedPreferencesProvider sharedPreferencesProvider;

    @Inject
    public Presenter(SharedPreferencesProvider prefs) {
        this.sharedPreferencesProvider = prefs;
    }

    public void setStartSleepingTime(long startSleepingTime) {
        sharedPreferencesProvider.setStartSleepingTime(startSleepingTime);
    }

    public long getStartSleepingTime() {
        return sharedPreferencesProvider.getStartSleepingTime();
    }

}