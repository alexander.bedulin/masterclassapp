package com.test.elenabedulina.masterclassapp.mvp.appdata;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Activity moduleActivity;

    public ActivityModule(Activity activity) {
        moduleActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return moduleActivity;
    }

    @Provides
    Activity provideActivity() {
        return moduleActivity;
    }
}