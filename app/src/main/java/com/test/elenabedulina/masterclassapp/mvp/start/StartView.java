package com.test.elenabedulina.masterclassapp.mvp.start;

import com.arellomobile.mvp.MvpView;

public interface StartView extends MvpView {
    void openLoginScreen();

    void openMainScreen();
}
