package com.test.elenabedulina.masterclassapp.mvp.sharedpreferences;

public interface Preferences {

    void setStartSleepingTime(long startSleepingTime);

    long getStartSleepingTime();
}
