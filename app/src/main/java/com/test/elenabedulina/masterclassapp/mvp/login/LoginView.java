package com.test.elenabedulina.masterclassapp.mvp.login;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragmentView;

public interface LoginView extends BaseFragmentView {

    @StateStrategyType(SkipStrategy.class)
    void openForgotPasswordScreen();

    void openMainScreen();

    void openRegistrationScreen();

    void showWrongPasswordMessage();

    void showEmptyEmailMessage();

    void showEmptyPasswordMessage();
}
