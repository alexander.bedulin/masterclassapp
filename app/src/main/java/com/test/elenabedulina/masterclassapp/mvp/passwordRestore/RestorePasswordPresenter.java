package com.test.elenabedulina.masterclassapp.mvp.passwordRestore;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragmentPresenter;

@InjectViewState
public class RestorePasswordPresenter extends BaseFragmentPresenter<RestorePasswordView> {

    void onRestorePasswordClick(@NonNull String emailAddress) {
        if(checkInput(emailAddress)){
            restorePassword(emailAddress);
        }
    }

    private void restorePassword(@NonNull String emailAddress){
        getViewState().showProgressBar();
        initFarebase().sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(task -> {
                    getViewState().hideProgressBar();
                    if (task.isSuccessful()) {
                        getViewState().showCheckEmailMessage();
                    } else if (task.getException() instanceof FirebaseAuthInvalidUserException) {
                        getViewState().showEmailIsWrongMessage();
                    } else {
                        getViewState().showErrorMessage();
                    }
                });
    }

    private boolean checkInput(@NonNull String emailAddress){
        boolean checkEditTexts = true;
        if (emailAddress.isEmpty()) {
            getViewState().showEmptyEmailMessage();
            checkEditTexts = false;
        }
        return checkEditTexts;
    }
}
