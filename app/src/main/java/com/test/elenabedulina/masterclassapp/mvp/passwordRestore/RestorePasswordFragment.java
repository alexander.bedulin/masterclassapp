package com.test.elenabedulina.masterclassapp.mvp.passwordRestore;

import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.test.elenabedulina.masterclassapp.R;
import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class RestorePasswordFragment extends BaseFragment implements RestorePasswordView {
    @InjectPresenter
    RestorePasswordPresenter mvpPresenter;

    @BindView(R.id.ed_email_restore_password)
    EditText edEmail;

    @Override
    public int getResourceId() {
        return R.layout.restore_password_layout;
    }

    @Override
    public void showEmailIsWrongMessage() {
        Toast.makeText(getContext(), R.string.wrong_email, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showCheckEmailMessage() {
        Toast.makeText(getContext(), R.string.mail_to_restore, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyEmailMessage() {
        edEmail.setError(getString(R.string.no_email));
    }

    @OnClick(R.id.btn_restore_password)
    void onRestorePasswordClick() {
        mvpPresenter.onRestorePasswordClick(edEmail.getText().toString());
    }
}
