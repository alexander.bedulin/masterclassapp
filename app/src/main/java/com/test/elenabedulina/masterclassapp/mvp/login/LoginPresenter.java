package com.test.elenabedulina.masterclassapp.mvp.login;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.test.elenabedulina.masterclassapp.mvp.baseMVP.BaseFragmentPresenter;

@InjectViewState
public class LoginPresenter extends BaseFragmentPresenter<LoginView> {

    void onForgotPasswordClick() {
        getViewState().openForgotPasswordScreen();
    }

    void onSignUpClick() {
        getViewState().openRegistrationScreen();
    }

    void onLoginClick(@NonNull String email, @NonNull String password) {
        if ( checkInput(email, password)){
            loginUser(email, password);
        }
    }

    private boolean checkInput(@NonNull String email, @NonNull String password) {
        boolean checkEditTexts = true;
        if (email.isEmpty()) {
            getViewState().showEmptyEmailMessage();
            checkEditTexts = false;
        }
        if (password.isEmpty()) {
            getViewState().showEmptyPasswordMessage();
            checkEditTexts = false;
        }
        return checkEditTexts;
    }

    private void loginUser(@NonNull String email, @NonNull String password) {
        getViewState().showProgressBar();
        initFarebase().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    getViewState().hideProgressBar();
                    if (!task.isSuccessful()) {
                        if (task.getException() instanceof FirebaseAuthInvalidUserException ||
                                task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            getViewState().showWrongPasswordMessage();
                        } else {
                            getViewState().showErrorMessage();
                        }
                    } else {
                        getViewState().openMainScreen();
                    }
                });

    }
}
